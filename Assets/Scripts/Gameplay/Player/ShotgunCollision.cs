﻿using UnityEngine;
using System.Collections.Generic;
using UnityStandardAssets.Characters.ThirdPerson;

public class ShotgunCollision : MonoBehaviour {

    [SerializeField] private GameObject particleEffect1;
    [SerializeField] private GameObject particleEffect2;
    [SerializeField] private GameObject particleEffect3;

    void OnTriggerEnter(Collider other)
    {
        
        if(other.tag == "Enemy")
        {

            GameObject deathEffect = (GameObject)Instantiate(particleEffect1, other.transform.position, other.transform.rotation);
            deathEffect.transform.position = new Vector3(other.transform.position.x, 3.2f, other.transform.position.z);
                        
            ScoreManager.ScoreIncreaseSmall();
            ScoreManager.KillCounter();

            Destroy(other.gameObject);
            Destroy(deathEffect, 3);
        }

        if (other.tag == "Enemy2")
        {
            GameObject deathEffect = (GameObject)Instantiate(particleEffect2, other.transform.position, other.transform.rotation);
            deathEffect.transform.position = new Vector3(other.transform.position.x, 3.2f, other.transform.position.z);

            ScoreManager.ScoreIncreaseMedium();
            ScoreManager.KillCounter();

            Destroy(other.gameObject);
            Destroy(deathEffect, 3);
        }

        if (other.tag == "Enemy3")
        {
            other.GetComponent<AICharacterControl>().life--;

            if (other.GetComponent<AICharacterControl>().life <= 0)
            {
                GameObject deathEffect = (GameObject)Instantiate(particleEffect3, other.transform.position, other.transform.rotation);
                deathEffect.transform.position = new Vector3(other.transform.position.x, 3.2f, other.transform.position.z);

                ScoreManager.ScoreIncreaseLarge();
                ScoreManager.KillCounter();

                Destroy(other.gameObject);
                Destroy(deathEffect, 3);
            }

        }

    }

    //private void Dead(Collider other, Vector3 hitPoint, Vector3 hitDirection)
    //{
    //    GameObject deathEffect = (GameObject)Instantiate(particleEffect1, hitPoint, Quaternion.FromToRotation(Vector3.forward, hitDirection));

    //    // move the rigidbody up so it does not intersect with the ground collider on spawn (causes wierd behaviour otherwise)
    //    deathEffect.transform.position = new Vector3(other.transform.position.x, 0.2f, other.transform.position.z);

    //    Destroy(other.gameObject);
    //    Destroy(deathEffect, 5);
    //}

}
