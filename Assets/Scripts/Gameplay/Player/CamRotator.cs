﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamRotator : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float initPosition;
    // Start is called before the first frame update
    void Start()
    {
        initPosition = Random.Range(0, 270);
        transform.rotation = Quaternion.Euler(0, initPosition, 0);
    }

    
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, speed * Time.deltaTime, 0);
    }
}
