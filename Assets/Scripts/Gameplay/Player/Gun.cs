﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class Gun : MonoBehaviour {

    [SerializeField] private GameObject buckshotPrefab;

    [SerializeField] private Transform barrel;
    [SerializeField] private GameObject collisionCone;

    [SerializeField] private Vector3 sightVector;
    [SerializeField] private float range;

    [SerializeField] private float fireRate = 1.5f;
    private float nextFire = 0.0f;

    private AudioSource fireSound;
    private AudioSource reloadSound;


    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        AudioSource[] tracks = GetComponents<AudioSource>();
        fireSound = tracks[0];
        reloadSound = tracks[1];

    }

	void Update () {
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            Fire();
        }
	}

    private void Fire()
    {
        // create the shooting particle effect
        nextFire = Time.time + fireRate;
        GameObject emitter = (GameObject)Instantiate(buckshotPrefab);
        emitter.transform.SetParent(barrel, false);
        emitter.transform.localPosition = Vector3.zero;
        emitter.transform.localEulerAngles = Vector3.zero;
        Destroy(emitter, 2);

        anim.Play("Fire", -1, 0);

        //Play Sound Files
        fireSound.Play();
        reloadSound.PlayDelayed(0.35f);

        // Create the collision cone to see if the enemies are inside the collider
        GameObject cone = (GameObject)Instantiate(collisionCone);
        cone.transform.SetParent(barrel, false);
        cone.transform.localPosition = Vector3.zero;
        cone.transform.localEulerAngles = Vector3.zero;
        Destroy(cone, 0.1f);
    }
}
