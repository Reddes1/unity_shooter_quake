﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameOverMenuController : MonoBehaviour
{
    [SerializeField] private Button nxtBtn;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI killText;
    [SerializeField] private TextMeshProUGUI timeText;
    [SerializeField] private TextMeshProUGUI finalText;
    private string scoreTemp;


    private void Start()
    {
        // Add a callback to the button, when the button is clicked the PlayGame() function will be called on the game manager.
        nxtBtn.onClick.AddListener(() =>
        {
            GameManager.Instance.HighScores();
        });




        //// Add a callback to the button, when the button is clicked the QuitGame() function will be called on the game manager.
        //quitBtn.onClick.AddListener(() =>
        //{     ScoreManager.Reset();
        //    GameManager.Instance.QuitGame();
        //});

        //highScoreBtn.onClick.AddListener(() =>
        //{
        //    GameManager.Instance.HighScores();
        //});

        scoreText.text = ScoreManager.score.ToString();
        killText.text = ScoreManager.killCounter.ToString();
        timeText.text = ScoreManager.timeBonusRD2.ToString();
        finalText.text = ScoreManager.finalScore.ToString();

    }
}