﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreMenuController : MonoBehaviour
{
    [SerializeField] private Button menuBtn;
    [SerializeField] private Button quitBtn;

    private void Start()
    {

        menuBtn.onClick.AddListener(() =>
        {
            GameManager.Instance.LoadMenuFromHighScore();
            ScoreManager.Reset();
        });

        quitBtn.onClick.AddListener(() =>
        {
            GameManager.Instance.QuitGame();
        });
    }
}