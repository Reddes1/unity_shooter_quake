﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InputWindow : MonoBehaviour
{
    [SerializeField] private Button okBtn;
    [SerializeField] private Button cancelBtn;
    [SerializeField] private TextMeshProUGUI titleText;
    [SerializeField] private TMP_InputField inputField;
    [SerializeField] private HighscoreTable highScoreTable;
    

    
    private float fScore;


    private void Awake()
    {
        highScoreTable.Hide();
        gameObject.SetActive(true);
               
    }

    public void OnClickAddName()
    {
        fScore = ScoreManager.finalScore;
        name = inputField.text;

        Hide();
        highScoreTable.AddHighscoreEntry((int)fScore, name);
        highScoreTable.Show();
        
    }

    public void Show()
    {
        gameObject.SetActive(true);

    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
