﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RulesControlsMenuController : MonoBehaviour
{
    [SerializeField] private Button rtnBtn;
    [SerializeField] private Button startBtn;

    private void Start()
    {
       
        rtnBtn.onClick.AddListener(() =>
        {
            GameManager.Instance.ReturnFromRules();
            Destroy(gameObject);
        });

        startBtn.onClick.AddListener(() =>
        {
            GameManager.Instance.PlayGame();
        });
    }
}