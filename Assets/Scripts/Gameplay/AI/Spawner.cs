﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    [SerializeField] private float minTime = 1.5f;
    [SerializeField] private float maxTime = 2.5f;

    [SerializeField] private Transform player;
    [SerializeField] private GameObject[] enemyPrefabs;
    [SerializeField] private Transform[] spawnPoints;
    private int randomSpawn;

    private float timeDelta = 0;

    void Start()
    {
        //Debug.Assert(enemyPrefabs[] != false, "Enemy prefab unassigned");
        timeDelta = Random.Range(minTime, maxTime);
    }

    void Update()
    {
       

        if (ScoreManager.score >= 350)
        {
            minTime = 0.2f;
            maxTime = 0.2f;
        }

        else if (ScoreManager.score >= 200)
        {
            minTime = 0.5f;
            maxTime = 0.5f;
        }

        else if (ScoreManager.score >= 150)
        {
            minTime = 1f;
            maxTime = 1.5f;
        }

        else if (ScoreManager.score >= 100)
        {
            minTime = 1.35f;
            maxTime = 2.0f;
        }

        else if (ScoreManager.score >= 50)
        {
            minTime = 1.35f;
            maxTime = 2.5f;
        }

        // Whilst timeDelta is above zero, we minus the time between the last update calls then check timeDelta again
        // if it is below zero we know the correct time has passed and we spawn an enemy from one of four random spawn
        // points. We then reset timeDelta to a random value.

        if (timeDelta > 0)
        {
            timeDelta -= Time.deltaTime;
            if(timeDelta <= 0)
            {
                int randomPoint = Random.Range(0, spawnPoints.Length);
                Vector3 pos = spawnPoints[randomPoint].position;
                GameObject enemy = Spawn(pos);
                if (enemy.tag == "Enemy3")
                {
                    enemy.GetComponent<GnomeAIBigBoi>().SetTarget(player);
                }
                else
                {
                    enemy.GetComponent<GnomeoAIController>().SetTarget(player);
                }
                timeDelta = Random.Range(minTime, maxTime);
            }
        }

    }

    public GameObject Spawn(Vector3 pos)
    {   
        // Spawns speedy and tanky mobs
        if (ScoreManager.score >= 100)
        {
            randomSpawn = Random.Range(1, enemyPrefabs.Length);
        }
        // Spawns all types of mobs (normal, speedy, tanky)
        else if (ScoreManager.score >= 50)
        {
            randomSpawn = Random.Range(0, enemyPrefabs.Length);
        }
        // Spawns normal and speedy mobs
        else
        {
            randomSpawn = Random.Range(0, enemyPrefabs.Length - 1);
        }

        return (GameObject)Instantiate(enemyPrefabs[randomSpawn], pos, Quaternion.identity);
    }
}
