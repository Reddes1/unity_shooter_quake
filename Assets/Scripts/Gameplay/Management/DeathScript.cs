﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScript : MonoBehaviour
{

    [SerializeField] private GameObject rotatorCam;
    [SerializeField] private GameObject deathAnim1;
    private static GameObject playerObj;

    GameObject deathEffect;
    GameObject camEffect;
    bool spawned = false;

    static private DeathScript instance = null;
    // Start is called before the first frame update
    public static DeathScript Instance
    {
        get
        {
            return instance;
        }
    }

    // Ensure there is only one instance of this object in the game
    void Awake()
    {
        if (instance != null)
            Destroy(gameObject);

        instance = this;
    }

    public void Death()
    {
        playerObj = GameObject.Find("Player");

        if (spawned == false)
        {
            deathEffect = (GameObject)Instantiate(deathAnim1, playerObj.transform.position, playerObj.transform.rotation);
            camEffect = (GameObject)Instantiate(rotatorCam, playerObj.transform.position, playerObj.transform.rotation);
            spawned = true;
        }
        //Destroy(playerObj);

        Invoke("PassBack", 3); // Delay for animation to play
    }

    public void PassBack()
    {
        spawned = true;
        GameManager.Instance.GameOver();
    }
}
