﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    enum GameState { START, IN_GAME, GAME_OVER, RULES, HIGHSCORES };
    private GameState gameState;
    [SerializeField] private GameState startState = GameState.START; // Exists to enable individual level testing
    [SerializeField] private GameObject gameOverPrefab;
    [SerializeField] private GameObject rulesPrefab;
    private Transform instance1;
    static private GameManager instance = null;
    
    // Lets other scripts find the instance of the game manager
    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    // Ensure there is only one instance of this object in the game
    void Awake()
    {
        if (instance != null)
            Destroy(gameObject);

        instance = this;
    }

    void Start()
    {
        gameState = startState;
    }

    void OnChangeState(GameState newState)
    {
        if (gameState != newState)
        {
            switch (newState)
            {
                case GameState.START:
                    
                    
                    break;
                case GameState.IN_GAME:

                    Time.timeScale = 1; // Set timescale to be a normal rate 
                    SceneManager.LoadScene("Game"); // Load the 'Game' scene

                    Cursor.lockState = CursorLockMode.Locked; // Lock the cursor to the game screen
                    Cursor.visible = false;

                    break;
                case GameState.GAME_OVER:

                    ScoreManager.CalcScore();
                    Cursor.lockState = CursorLockMode.None; // unlock the cursor for the menu
                    Cursor.visible = true;

                    Time.timeScale = 0;
                    Transform instance = Instantiate(gameOverPrefab).transform; // Instantiate the GameOver menu prefab

                    break;
                case GameState.RULES:
                      
                    instance1 = Instantiate(rulesPrefab).transform;
                   
                    break;
                case GameState.HIGHSCORES:

                    Cursor.lockState = CursorLockMode.None; // unlock the cursor for the menu
                    Cursor.visible = true;

                    SceneManager.LoadScene(2);
                    break;
            }

            gameState = newState;
        }
    }

    private void EnableInput(bool input)
    {
        // Find the player object
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<FirstPersonController>().enabled = input;
        player.GetComponentInChildren<Gun>().enabled = input;
    }

    public void PlayGame()
    {
        OnChangeState(GameState.IN_GAME);
    }

    public void GameOver()
    {
        OnChangeState(GameState.GAME_OVER);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void HighScores()
    {
        OnChangeState(GameState.HIGHSCORES);
    }

    public void Rules()
    {
        OnChangeState(GameState.RULES);
    }

    public void ReturnFromRules()
    {
        OnChangeState(GameState.START);
        
    }

    public void StartGameFromHighScore()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadMenuFromHighScore()
    {
        SceneManager.LoadScene(0);
    }

    public void GameOverPart1()
    {
        EnableInput(false); // disable character
        DeathScript.Instance.Death();
    }
}



