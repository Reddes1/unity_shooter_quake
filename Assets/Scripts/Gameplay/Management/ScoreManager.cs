﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{

    [SerializeField] private Text scoreTxt;
    [SerializeField] private Text killTxt;
    [SerializeField] private Text timeTxt;
    

    public static float timeBonus;
    public static int timeBonusRD2;
    public static int score;
    public static int killCounter;
    public static int finalScore;
    Scene scene;
       
    static private ScoreManager instance = null;
    // Start is called before the first frame update
    public static ScoreManager Instance
    {
        get
        {
            return instance;
        }
    }
    // Start is called before the first frame update

    void Awake()
    {
        if (instance != null)
            Destroy(gameObject);

        instance = this;
    }

    void Start()
    {
        score = 0;
        killCounter = 0;
        timeBonus = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        //Check current scene name
        scene = SceneManager.GetActiveScene();

        if (scene.name == "Game")
        {
            
            scoreTxt.text = "Score: " + score;
            killTxt.text = "Kill Count: " + killCounter;

            timeBonus += Time.deltaTime;

            //timeTxt.text = "Time: " + timeBonusRD2;
        }
        
    }

    public static void ScoreIncreaseSmall()
    {
        score = score + 2;
    }

    public static void ScoreIncreaseMedium()
    {
        score = score + 5;
    }

    public static void ScoreIncreaseLarge()
    {
        score = score + 10;
    }

    public static void KillCounter()
    {
        killCounter++;
    }

    public static void Reset()
    {
        score = 0;
        killCounter = 0;
        timeBonus = 0;
    }

    public static void CalcScore()
    {
        timeBonusRD2 = Mathf.RoundToInt(timeBonus);

        finalScore = score + timeBonusRD2 + killCounter;
    }
}
