﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{
    private AudioSource menuTrack;
    private AudioSource gameTrack;

    private bool _pauseSound;
    static private SoundManager instance = null;
    Scene scene;


    public static SoundManager Instance
    {
        get
        {
            return instance;
        }
    }

    // Ensure there is only one instance of this object in the game
    void Awake()
    {
        if (instance != null)
            Destroy(gameObject);

        instance = this;
    }

    public bool PauseSound
    {
        get
        {
            return _pauseSound;
        }
        set
        {
            _pauseSound = value;

            AudioSource[] sources = FindObjectsOfType<AudioSource>();

            foreach (AudioSource source in sources)
            {
                if (source.gameObject != gameObject)
                {
                    if (value == true)
                    {
                        source.Pause();
                    }
                    else
                    {
                        source.Play();
                    }
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        AudioSource[] tracks = GetComponents<AudioSource>();
        menuTrack = tracks[0];
        gameTrack = tracks[1];
    }

    private void Update()
    {
        scene = SceneManager.GetActiveScene();

        if (scene.name == "Game")
        {
            if (!gameTrack.isPlaying) 
                gameTrack.Play();

            if (menuTrack.isPlaying)
                menuTrack.Stop();
        }

        if (scene.name == "Start")
        {
            if (!menuTrack.isPlaying)
                menuTrack.Play();

            if (gameTrack.isPlaying)
                gameTrack.Stop();
        }

        if (scene.name == "HighScores")
        {
            if (!menuTrack.isPlaying)
                menuTrack.Play();

            if (gameTrack.isPlaying)
                gameTrack.Stop();
        }

    }


    public void PlayMenuTrack()
    {
        if (menuTrack.isPlaying) return;

        menuTrack.Play();

    }

    public void PlayGameTrack()
    {
        
    }
}
